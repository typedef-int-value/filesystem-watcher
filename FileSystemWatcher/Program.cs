﻿using System;
using System.IO;

namespace watcher
{
  class main
  {
    static void Main(string[] args)
    {
      FileSystemWatcher watcher = new FileSystemWatcher();
      Console.WriteLine("from path: (ex: c:\\)");
      //string fromPath = @"c:\";
      string fromPath = Console.ReadLine();
      FileAttributes attr = File.GetAttributes(fromPath);
      if (!((attr & FileAttributes.Directory) == FileAttributes.Directory)) {
        Console.WriteLine("path is not a directory");
        return;
      } 

      if (fromPath[fromPath.Length-1] != '\\') {
        fromPath += '\\';
      }

      string toPath = @".\FileSystemWatcher\";

      watcher = new FileSystemWatcher { Path = fromPath, IncludeSubdirectories = true, Filter = "*.*" };
      watcher.EnableRaisingEvents = true;
      watcher.Changed += new FileSystemEventHandler(WatcherChanged);

      void WatcherChanged(object source, FileSystemEventArgs e)
      {
        string sourceFileName = fromPath + e.Name;
        string destFileName = toPath + e.Name;
        
        try
        {
          (new FileInfo(destFileName)).Directory.Create();
          File.Copy(sourceFileName, destFileName, true);
          Console.WriteLine("from: " + sourceFileName + "\nto: " + destFileName);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message); // Probably a directory not a file.
        }
      }
      Console.WriteLine("watch: " + fromPath);

      // Wait for user to hit enter to end copying
      Console.ReadLine();
    }


  }
}
